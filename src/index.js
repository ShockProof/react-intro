import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

class Square extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      row : null,
      col : null
    }
  }
  
  update() {
    this.setState({
      row : this.props.row,
      col : this.props.col
    });
  }

  render() {
    return (
      <button className="square" onClick={()=>this.update()} >
        { 
          this.state.row!=null ? ( this.state.row + ',' + this.state.col ) : null
        }
      </button>
    );
  }
}

class Row extends React.Component {
  render() {
    return (
      <div className="board-row">
        <Square row={this.props.rowNumber} col={0}/>
        <Square row={this.props.rowNumber} col={1}/>
        <Square row={this.props.rowNumber} col={2}/>
      </div>  
    );
  }
}

class Board extends React.Component {
  renderSquare(i,j) {
    return <Square row={i} col={j} />;
  }

  render() {
    const status = 'Next player: X';
    return (
      <div>
        <div className="status">{status}</div>
        <Row rowNumber={0} />
        <Row rowNumber={1} />
        <Row rowNumber={2} />
      </div>
    );
  }
}

class Game extends React.Component {
  render() {
    return (
      <div className="game">
        <div className="game-board">
          <Board />
        </div>
        <div className="game-info">
          <div>{/* status */}</div>
          <ol>{/* TODO */}</ol>
        </div>
      </div>
    );
  }
}

// ========================================

ReactDOM.render(
  <Game />,
  document.getElementById('root')
);
